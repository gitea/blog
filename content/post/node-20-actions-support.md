---
date: 2023-09-24T00:00:00+00:00
authors: "techknowlogick"
title: "Gitea Actions now Supports Node20 based actions"
tags: ["release", "actions"]
draft: false
---

With the End-of-life of Node 16, we have now added Node 20 support to Gitea Actions. This is thanks in part to [freshollie](https://github.com/nektos/act/pull/1988), the nektos/act maintainers, [SupremeVoid](https://gitea.com/SupremeVoid), and the Gitea team.

To be able to use Node 20 actions, you'll need to update your runner to version v0.2.6.

If you run into any issues, or have any questions, please reach out to us via our [many support channels](https://docs.gitea.com/help/support)
