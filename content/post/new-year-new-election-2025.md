---
date: 2025-01-28T10:10:00+00:00
authors: "techknowlogick"
title: "New Year, New Election"
tags: ["quarterly", "election"]
draft: false
---

As is tradition, we start each year with a new election for our Technical Oversight Committee. This past year, the elected members were (in alphabetical order) [6543](https://gitea.com/), [delvh](https://gitea.com/), and [jolheiser](https://gitea.com/). We'd like to thank them for their service in continuing to ensure that Gitea is the best it can be.

Thanks to the leadership of jolheiser, our election process started this past November where maintainers nominated other maintainers, or themselves to be put up as options for the election. The candidates who accepted the nominations were, [6543](https://gitea.com/6543), [delvh](https://gitea.com/delvh), [lafriks](https://gitea.com/lafriks), [tboerger](https://gitea.com/tboerger), and [yp05327](https://gitea.com/yp05327). Sadly, yp05327 withdrew from consideration, and we wish them the best. The voting platform we used (heliosvoting.org) allowed for self-spoling ballots, so warning was provided to those who may have voted for yp05327 to change their ballot should they wish to.

This years elected TOC members are, incumbents 6543 and delvh, and also we are welcoming back lafriks who was previously an elected project owner for the years 2018-2021.

The ballot totals are[^1]:
```
6543       13
delvh      15
lafriks    15
tboerger   7
yp05327    5
```

We look forward to our first meeting of the TOC to set the roadmap for the upcoming year.

Thank you to everyone for your participation in this wonderful journey of building Gitea.

Happiest of New Years to all!

[^1]: Voting details can be found at https://vote.heliosvoting.org/helios/e/gitea-toc-2025